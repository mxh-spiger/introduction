# 其他推荐软件及环境准备

## 1. 工具软件

### 1.1 VScode

> vscode是一款轻量功能丰富的代码编辑工具，具有代码高亮、补全，语法检测，批量操作，调试debug等等方便的功能，建议安装使用

1. 下载[windows vscode安装包](https://vscode.download.prss.microsoft.com/dbazure/download/stable/0ee08df0cf4527e40edc9aa28f4b5bd38bbff2b2/VSCodeUserSetup-x64-1.85.1.exe)
2. 下载结束后，双击exe文件开始安装，安装目录可以按照默认，开始菜单文件夹也默认即可<br>        <img src = "assets/image-20231221100801934.png">
3. <img src = "assets/image-20231221100813294.png">
4. 附加任务，推荐全部选择<img src = "assets/image-20231221100512579.png">
5. 选择”安装“，稍加等待即可
6. 推荐扩展插件安装：
   1. Chinese（Simplified）将 VSCode 操作界面转换为中文，重启vscode生效<img src = "assets/image-20231221101517256.png">
   2. Code Runner、WSL、python、pylance等代码运行、高亮提示、docker扩展等实用工具	     <img src = "assets/image-20231220155658262.png">
   
      <img src = "assets/image-20231225170352697.png">
      <img src = "assets/image-20231225170418847.png">

### 1.2 visual studio 2022

1. 下载[社区版visual studio 2022在线安装包](https://visualstudio.microsoft.com/zh-hans/thank-you-downloading-visual-studio/?sku=Community&channel=Release&version=VS2022&source=VSLandingPage&cid=2030&passive=false)
2. 下载结束后，我们双击开始安装<img src = "assets/image-20231221093612513.png">
3. 稍加等待<img src = "assets/image-20231221094000219.png">
4. 安装程序准备完成后会自动弹出以下窗口，根据需要选择功能集和工作负载，同时选择安装路径（建议使用默认安装路径），这里我们按图示选择即可，在窗口的上方还有单个组件、语言包、安装位置这些选项，可以不用管，均为默认值。最后点击安装<img src = "assets/image-20231221092814353.png">
5. 接着慢慢等待安装进度条走完即可，安装完毕后需要重启计算机
6. 重启之后，右键可以看到多了一个使用VIsual Studio打开<br><img src = "assets/image-20231221095605546.png">

### 1.3 Git

为了简化安装过程，推荐安装包管理器[chocolatey](https://chocolatey.org/install)

使用choco命令安装软件的时候需要管理员权限打开powershell，非常的不方便。这里推荐大家安装以下gsudo，它类似于linux系统上的sudo命令，能够以管理员的权限执行后面的命令

```PowerShell
# 最后一次以管理员打开powershell，执行
choco install gsudo
```

此后我们便可以在普通的powershell窗口下使用choco安装软件了。安装Git：

```PowerShell
# 打开powershell，执行
gsudo choco install git
```

### 1.4 Docker的使用

#### Docker Desktop软件的安装

> docker desktop是windows的docker软件，可以方便的使用开发人员提供的docker免去配置环境的过程

下载[Docker Desktop安装包](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)，选项均选择默认即可

#### 导出镜像与创建容器

1. 在安装好`docker Desktop`的环境中打开`powershell/cmd`

2. 进入存放`docker`镜像文件 `Icraft_3.0.0.tar` 的路径下 

3. 依次运行（此处以`Icraft3.0`版本提供的`docker`名称为例，若使用其他版本的`docker`，请查看注释中的变量含义并进行相应替换）：

```cmd
# 载入镜像到本地:
# modelzoo_v3.0.0_image		:	可以起任意名称,作为该镜像在本地的名字
docker import Icraft_3.0.0.tar modelzoo_v3.0.0_image

# 将镜像映射为容器，开启容器，进入容器中的bash:
# modelzoo_v3.0.0_container	:	可以起任意名称，作为该容器在本地的名字
# E:\docker_share			:	是本地任意一个存在的路径，作为宿主机与容器文件交互的传送门
# /mnt/SHARE				:	是容器内的路径，作为容器文件与宿主机文件交互的传送门
# modelzoo_v3.0.0_image		: 	使用哪个镜像来映射为容器，使用上一条命令中给镜像起的名字即可
docker run --name=modelzoo_v3.0.0_container -it -v E:\docker_share:/mnt/SHARE modelzoo_v3.0.0_image /bin/bash	

# 如果执行docker run时产生以下报错——
# docker: Error response from daemon: invalid mode: /mnt/SHARE.
# 请先cd到你想要作为宿主机与容器文件交互的传送门的文件夹，然后把文件夹的绝对路径替换成$PWD，如下所示
# docker run --name=try_v3.0.0_container -it -v $PWD:/mnt/SHARE modelzoo_v3.0.0_image /bin/bash
```

**Tips:**  如果您正在使用的是`Icraft3.0`的`docker`，请注意——开启容器进入bash后，需要执行`source /etc/profile`命令，以将一些编译所需的文件加入环境变量，不然交叉编译的`cmakelist`中无法找到所需的环境变量

#### 交叉编译

```cmd
# AXI工程编译
cd /home/fmsh/ModelZoo/tutorial-runtime/runtime3.0/demo/build
rm -rf *
cmake ..
make -j
```




## 2. 上板环境

### 2.1 串口驱动安装

不同系统对应不同版本的串口驱动，请根据自己电脑的windows版本下载对应的安装包：

#### windows7

下载[串口驱动win7版本安装包](https://www.silabs.com/documents/public/software/CP210x_Windows_Drivers.zip)，将下载的zip解压后，双击电脑对应架构的exe即可安装

#### windows10

1. 下载[串口驱动win10版本安装包](https://www.silabs.com/documents/public/software/CP210x_Universal_Windows_Driver.zip)，并解压
2. 驱动安装之前，请确保设备从电脑拔除
3. 解压zip压缩包，右键 ``silabser.inf``文件，选择安装<br><img src="assets/image-20231219163410714.png" >
4. 安装完成：<img src="assets/image-20231219163002044.png" >
5. 安装完成后需要重启电脑

### 2.2 串口调试工具Tera Term

1. 从下载zhan下载[Tera Term安装包](https://download.fdwxhb.com/data/04_FMSH-100AI/100AI/04_modelzoo/modelzoo_pub/pkgs/serial_tool/teraterm-5.0.exe)
2. 安装时均选择默认选项即可
3. 将串口的usb端插入电脑，另一端插入 ``psin``接口，打开电脑设备管理器，确认端口号
4. 打开Tera Term，点击此处新建连接<br><img src = "assets/image-20231220100514351.png">
5. 选择串口，选择刚才在设备管理器中查看到的端口号，即可开始串口调试

   `<img src = "assets/image-20231220100837097.png">

### 2.3 修改板子ip

1. 首先 `sudo -i`，然后进入 `/etc/rc.local`
2. 由路由器分配 `ip`

```
#!/bin/bash
ifconfig eth0 down
macchanger -m 20:5f:52:aa:cc:44 eth0
ifconfig eth0 up

systemctl start systemd-resolved

dhclient

systemctl start sshd
```

`mac`号可随机生成，设置好后，通过串口连板子，再使用 `ifconfig`即可查看分配到的 `ip`

3. 设置静态 `ip`（容易 `ip`冲突，一般用于 `pc`直连板子）

```
#!/bin/bash
ifconfig eth0 192.168.125.171 netmask 255.255.255.0
systemctl start sshd
```

**Tips：**如果使用 `pc`直接连板子（不是经过路由器连板子），则需要：

- 将 `pc`的 `ip`段调整到与 `sd`卡一致<img src = "assets/image-20231221161637790.png">
- 在以太网属性-配置-高级-链接速度和双工模式选择 `100mbps`全双工`

`<img src = "assets/image-20231221161944049.png">

<br>

<div style="text-align: center;"><img src = "assets/image-20231221162024176.png"></div>

### 2.4 远程调试工具mobaXterm

安装后可以通过**ssh**的方式连接和调试板子

1. 下载[mobaXterm安装包](https://download.mobatek.net/2342023101450418/MobaXterm_Installer_v23.4.zip)，或者从[下载站获取mobaXterm安装包](https://download.fdwxhb.com/data/04_FMSH-100AI/100AI/04_modelzoo/modelzoo_pub/pkgs/MobaXterm_Installer_v25.0.zip)，解压后双击 ``.msi``文件安装，选项均默认即可
2. 选择 ``Session——SSH``创建会话<img src = "assets/image-20231221112009516.png">
3. 在”Remote host“处填写板子的IP地址，勾选”Specify username“并输入用户名，如 ``root``<img src = "assets/image-20231221112258813.png">
4. 在弹出的页面中输入密码后，会在左侧边栏显示板子上的文件夹，后续将弹出的 ``master password``框可以选择性设置，如果选择 ``cancel``，需要每次连接时输入密码<img src = "assets/image-20231221113320326.png">
5. 在左侧边文件栏中，可以直接采用拖拽的方式，或者采用鼠标右键选择相应功能，进行文件传输和下载
6. 在 ``Settings``选项卡中选择 ``Configuration``，可以进行个性化设置。比如设置默认的文件下载路径、右键复制、字体和大小等

<img src = "assets/image-20231221142815777.png">

### 2.5 板子server

把server工程考到新卡上，如果报

```bash
error while loading shared libraries: libIcraftDevice.so: cannot open shared object file: No such file or directory
```

则需要把该so放到环境变量里：

```bash
export LD_LIBRARY_PATH=/root/IcraftServer:/root/Icraft-JDY_Camera/Deps/so.h2s
```

**Tips：**注意linux环境间用**冒号**间隔



## 3. vscode通过gdb server进行c++远程断点调试

1. **代码链接跳转等**

   在 `wsl`中安装 `vscode`的 `clangd`插件，对代码的提示功能比 `micro`自带的做的好一些（注意不能先装 `micro c++`的插件），如果没有装 `clangd server vscode`会提示安装；如果无法用 `vscode`自动安装 `clangdserver`可以 `apt-get install clangd`；安装完 `clangd`之后可能找不到头文件，可以在 `clangd setting`中加入 `--compile-commands-dir=${workspaceFolder}/build`

   打开一个工程后，先选择用哪个 `cmakelist`，然后选择 `g++`编译器版本；
2. **gdb远程断点调试：**

   1. 在板子上安装 `gdbserver`
   2. 在本地交叉编译环境安装本地架构的 `gdb`，比如 `gdb-multiarch`
   3. `cmake`时设置编译 `debug`版本

      ```cmake
      cmake_minimum_required(VERSION 3.10)
      project(BY-Yolo-AXI)
      
      SET(CMAKE_SYSTEM_NAME Linux)
      SET(CMAKE_SYSTEM_PROCESSOR aarch64)
      #------------------------------
      SET(CMAKE_BUILD_TYPE Debug)
      #------------------------------
      SET(CMAKE_CXX_STANDARD 17)
      SET(CMAKE_CXX_STANDARD_REQUIRED YES)
      SET(CMAKE_C_COMPILER /usr/bin/aarch64-linux-gnu-gcc)
      SET(CMAKE_CXX_COMPILER /usr/bin/aarch64-linux-gnu-g++)
      SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS} -O0 -g -Wall")
      SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -O0 -g -Wall")
      SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS} -O3")
      SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O3")
      
      include_directories(./Deps/include)
      
      link_directories(
          ./Deps/a
          ./Deps/so
      )
      
      link_libraries(icraft_tensor icraft_ir icraft_runtimeforward icraft_device icraft_runtime fmt stdc++fs opencv_imgcodecs opencv_imgproc opencv_highgui opencv_core tegra_hal libjpeg-turbo ittnotify libpng libwebp libtiff libopenjp2 zlib dl pthread)
      
      add_executable(YoloV3_8 YoloV3_8.cpp)
      add_executable(YoloV4_8 YoloV4_8.cpp)
      ```
   4. 配置本地 `vscode`调试配置，比如：

      ```json
      {
          "version": "0.2.0",
          "configurations": [
              {
                  "name": "(gdb)cpp",
                  "type": "cppdbg",
                  "request": "launch",
                  "program": "${workspaceFolder}/build/YoloV5n_8",
                  "args": [],
                  "stopAtEntry": false,
                  "cwd": "${workspaceFolder}",
                  "environment": [],
                  "externalConsole": true,
                  "MIMode": "gdb", 
                  "miDebuggerPath": "/usr/bin/gdb-multiarch",
                  "miDebuggerServerAddress": "192.168.125.134:2077",  
                  "setupCommands": [
                      {
                          "description": "Enable pretty-printing for gdb",
                          "text": "-enable-pretty-printing",
                          "ignoreFailures": true
                      }
                  ]
              }
          ]
      }
      ```
   5. 在板子上输入：

      `gdbserver :2077 ./YoloV5n_8`

      端口号与上述 `json`中一样即可
3. 在vscode中打断点并使用调试模式运行程序
