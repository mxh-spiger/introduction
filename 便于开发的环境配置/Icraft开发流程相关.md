# Icraft开发流程相关

## 1. python以及框架版本

python版本，及模型**导出时**使用的框架版本要求：

- `python == 3.8`（建议）
- `torch == 1.9`
- `tensorflow == 1.15`
- `onnx == opset11`
- `cuda == 11.7`

## 2. icraft版本切换

### 2.1 icraft3.1.0及以后

例如想要把版本从3.1.0更换为3.2.0，依次执行以下操作即可：

- 用**管理员身份**打开终端
- 执行 `icraft --version v3.2.0`

<img src = "assets/Snipaste_2024-04-23_14-43-22.png">

**Tips for usage：**

- 自动版本更换不包括**CustomOp版本的更换**，只更换icraft主体程序的版本，如果您需要更换CustomOp程序的版本，无需卸载旧版本，直接安装需要的目标版本，即可覆盖完成安装
- 如果您在windows上使用icraft的**pythonAPI**，安装的python包版本必须和当前icraft版本一致，不然会找不到
- 如果您开发时使用**VSCode**，需要注意icraft更换新版本后，vscode不能及时更新icraft的环境变量，可能导致虽然python包能够找到，但是vscode里icraft环境变量还是旧的，现象为跑pythonAPI的程序时既没有报错，也没有运行任何东西，建议新开一个终端来运行程序。如果仍然需要使用vscode，则重启电脑后，vscode的环境变量会更新为您新安装的icraft版本
- 如果您开发时使用**Visual Studio**，需要注意该软件不重启电脑可能也无法自动更新环境变量。所以Customop的版本如果变化了，visual studio里跑程序就会还是找原来的customop版本，从而找不到文件而引发错误。安装新版本后同样建议打开新的终端或重启电脑，以正常运行程序
- **linux**下还不支持icraft的版本切换，如果需要更换版本，无需卸载旧版本，直接安装新的目标版本即可覆盖。另外，icraft3.2之前的linux版本在安装后，需要 `source etc/profile`后才能使用；而从icraft3.2开始，您无需再进行这一操作，即可正常使用
- 如果您需要在wsl中进行**交叉编译**，记得在wsl中也安装新的icraft版本哦

### 2.2 icraft3.0.0、3.0.1

> 自3. 0版本开始，我们就希望提供方便大家使用的版本切换方式。但是在3.0.0和3.0.1上，这一项功能尚不稳定，有些情形下可能需要劳烦各位手动卸载与重装，才能完成版本的切换。

- 情形1：从3.0.1或3.0.0切换到3.1.0或之后的版本——只能手动卸载然后重新安装
- 情形2：从3.1.0或更高切换到3.0.1或3.0.0的版本——方法同[icraft3.1.0及以后](#2.1 icraft3.1.0及以后)

### 2.3 icraft2.x的版本切换

若希望在电脑中保留旧的icraft版本，并切换到新的版本，可以参考以下做法：

1. 先修改C盘中原 ``Icraft-CLI``文件夹的名称，增加版本后缀，由于安装的路径永远在 ``C:\Icraft-CLI``，改名可以防止安装新版本时把旧版本的文件覆盖

   **Tips：** 每次安装新的版本之前，都必须在修改完旧版本文件夹名称后，先安装一遍Rely，保证每个版本的文件夹内都有一份Rely

<img src = "assets/image-20231220143306134.png">

2. 通过命令行中输入 ``icraft -v``指令，确认当前运行版本

<img src = "assets/image-20231220143355137.png">

3. 需要重新使用旧版本时，用同样的方法，将不想用的版本文件夹加上后缀，将目标版本文件夹名称改为 ``Icraft-CLI``即可

## 3. 运行时c++工程的编译环境

### 3.1 使用 `windows cmake`本地编译

下载[`win`版 `cmake`安装包](https://github.com/Kitware/CMake/releases/download/v3.28.1/cmake-3.28.1-windows-x86_64.msi)，双击 ``.msi``文件即可安装

### 3.2 使用 `wsl cmake`交叉编译

> wsl是win10自带的子系统，由于当前版本的icraft必须在windows环境使用，因此装一个wsl有助于交叉编译和远程调试debug
>
> 推荐使用交叉编译，因为板上资源有限，编译会很慢，还有可能由于内存问题编译不成功

编译环境：

- os：ubuntu20.04

  - 首先需要安装：``wsl2.0 - ubuntu20.04``，wsl的安装方法可以参考 [WSL的安装与使用（超详细图文版安装教程）](https://blog.csdn.net/weixin_57367513/article/details/135001273)

- 装libdw（使用icraft3.x必须安装，使用icraft2.x可以不安装）

  - 下载[arm64.list](https://gitee.com/mxh-spiger/tutorial-runtime/blob/rt3.0.0/assets/arm64.list)
  - 放到指定位置：`sudo cp ./arm64.list /etc/apt/sources.list.d/`

  - 执行:`sudo dpkg --add-architecture arm64`

  - 将`/etc/apt/sources.list`里面的每一个deb后面添加方括号里的内容 `[arch=amd64,i386]`

  - 执行：`sudo apt update `

  - `sudo apt install libdw-dev:arm64`

  - 如果由于网络问题执行不了上述安装，可以直接放一些libdw的so库，因为交叉编译过程并不会真的调用。点击下载：[libdw](https://gitee.com/link?target=https%3A%2F%2Fdownload.fdwxhb.com%2Fdata%2F04_FMSH-100AI%2F100AI%2F04_modelzoo%2Fmodelzoo_pub%2Fpkgs%2Fcc_pkgs%2Flibdw%2F)，讲这些文件存入：`\wsl.localhost\Ubuntu-20.04\usr\lib` ,如果是其他操作系统存放到相应的位置。如果用这种方式编译其他工程提示找不到libdw中相关的so，例如：


  <img src = "assets/2024-06-06 10-33-57.png">

  可以根据warnning后的文件名，修改usr/lib中对应的文件名

- 装c++编译器

```bash
sudo apt-get install build-essential
```

- 装cmake

```
sudo apt install libssl-dev
wget https://github.com/Kitware/CMake/releases/download/v3.24.2/cmake-3.24.2.tar.gz
tar -zxvf cmake-3.24.2.tar.gz
cd cmake-3.24.2
./bootstrap
make
make install
```

- 装ninja

```
sudo apt install ninja-build
```

- 装交叉编译器

```
sudo apt install gcc-aarch64-linux-gnu
sudo apt install g++-aarch64-linux-gnu
```

**Tips:** 板子连外网设置

在rc.local中配置DHCP

```bash
timeout5 dhclient
```

测试是否连通：

```bash
ping baidu.com
```



## 4. Cuda仿真环境要求

运行python cuda仿真的环境要求
- cuda版本：11.7 	[cuda11.7 x86 windows10 离线安装包下载](https://developer.download.nvidia.com/compute/cuda/11.7.0/local_installers/cuda_11.7.0_516.01_windows.exe)
- 电脑系统： x86
- 显卡要求：能够支持cuda11.7的显卡

## 5. icraft2.2 仿真工程使用说明

### 5.1 使用流程

1. 点击[仿真工程链接](https://gitee.com/mxh-spiger/simulate)，将工程克隆到本地
2. 打开 ``modelzoo.sln``，确保为release模式 `<img src = "assets/image-20231222100830161.png">`
3. 确认c++版本为c++17

   ![1704717509268](assets/1704717509268.png)
4. 在解决方案资源管理器中，选中 ``modelzoo_sim``，右键选择”设为启动项目“`

```<img src = "assets/image-20231222093308985.png">``

5. 配置属性-调试修改：

- 设置配置文件路径：`simulate`提供了 `sim.yaml`例子
- 将 `thirdparty`的 `dll`加入环境中，`dll`主要用了 `yaml`，`simulate`等

  - ``PATH=%PATH%;../../thirdparty/dll;C:\Icraft-CLI\Rely;``
  - 由于工程的依赖涉及C盘安装的 `Icraft-CLI`，因此须确保目前安装的版本与工程相匹配，这里推荐使用 `Icraft2.2`版本，支持 `cudamode`仿真

  ```<img src = "assets/image-20231222102227717.png">``

6. 配置 `yaml`文件（后面写有 `yaml`文件配置方法详细举例）
7. 打开 `cudamode`（如果电脑没有显卡或打开cuda模式报错，可以关闭）

   a. 打开仿真工程下的 `forward.cpp`文件 `<br><img src = "assets/image-20231225103206232.png">``<br>`
   b. 找到待测网络对应的前处理函数，如 `yolov5s`则为 `yolo<br>``<img src = "assets/image-20231225110023014.png">`
   c. 在该函数中找到 `SET SIMULATOR`部分，如果电脑有显卡并安装了 `CUDA`，可以将 `cudamode`设置为 `true`加快仿真运行时间，否则需要设置成 `false<br>``<img src = "assets/image-20231225110137600.png">`
8. 经过以上步骤后，可以执行编译，过程中提示找不到 `ir virsion`相关头文件，可以把找不到的头文件的那行 `include`的代码注释掉

### 5.2 `yaml`文件配置说明

> 由于该工程里包含了多个模型的前后处理demo，使用时需要配置使用哪些模型，并且进行模型路径配置，后处理参数配置等

**用前注意：**

- 注意 `yaml`的冒号后面要留1个空格，不能多也不能少。以及不可随意修改缩进，容易影响层级关系，建议在 `sim.yaml`例子基础上修改，不要随意增改格式
- 仿真工程不能运行带硬算子的网络，因此编译用于仿真的 `ini`时，``imagemake``和 ``icorepost``需要去掉，如图（注释需要用井号“**#**”，不能用分号“**；**”）``<br><img src = "assets/image-20231225135233162.png">```

下面就以 `yolov5s`为例，在注释中详细介绍我们需要修改的各项配置

````yaml
# 全局参数配置，跑多个网络只需要修改此一处
conf: &conf 0.3
# 运行单例观察效果时，可如示例中，设置较高置信度作为阈值，精度测试时须设为0.001
show: &show true
# 是否显示结果图片，精度测试时关闭
lc_cocoval2017: &lc_cocoval2017 D:\datasets\val2017\
# 若待测网络使用coco数据集，可在此处统一配置测试集文件夹，否则不用配置
lc_cocoval2017_list: &lc_cocoval2017_list D:\datasets\cocoval_2017.txt
# 同上，txt文件为上面文件夹中的图片文件名列表
coconames: &coconames D:\Icraft\workspace_v2.0\icraft-tutorial\networks\names\coco.names
# 数据集类别名称文件
multilable: &multilable false
# 是否使用多标签检测，打开后一个目标可以记录多个类别，精度测试时须打开
tempres: &tempres D:\Icraft\restemp\temp
# 设置临时结果图片存放路径，精度测试时不推荐使用
````

```yaml
# 整体models部分，决定这次运行要跑哪些网络，分别使用什么前后处理
models: [
           [yolov5_sim,yolov5s_8,yolov5],
           # 对应关系：[前处理、模型前向json&raw及结果保存路径、后处理参数及测试集配置]
           #[yolov3_sim,yolov3_8,yolov3],
           # 此次不运行
           ]
```

```yaml
# 具体网络文件配置，此处以量化阶段举例，其实包括parse、optimize、quantize、adapt、generate的所有阶段都可以仿真
networks:
  yolov5s_8:
    jsonPath: E:\YoloV5s_8\YoloV5s_8_quantized.json
    # 编译生成的json文件
    rawPath: E:\YoloV5s_8\YoloV5s_8_quantized.raw
    # 编译生成的raw文件
    resRoot: *tempres
    # 结果图片存放路径，这里用了全局参数存放在临时文件夹，跑单张图时可以使用
    resRoot: E:\rsl\yolov5s_8_month_day\
    # 精度测试时，推荐配置为本地单独的文件夹，路径的父文件夹必须存在，路径末尾必须有“\”
    show: *show
    # 全局参数：是否显示结果图片
```

```yaml
# 后处理参数及测试集配置，若使用coco数据集的官方模型，一般不需要修改
params:
 yolov5:
  conf: *conf
  # 全局参数：置信度
  iou: 0.65
  # 重叠度，与浮点精度测试时使用的数值对齐即可
  number_of_class: 80
  # 类别数
  number_of_head: 3
  # 检测头数量
  anchors: [[[10.,13.],[16.,30.],[33.,23.]],[[30.,61.],[62.,45.],[59.,119.]],[[116.,90.],[156.,198.],[373.,326.]]]
  # 锚框
  multilable: *multilable
  # 全局参数：多标签检测，精度测试时打开
  imgRoot: *lc_cocoval2017
  # 全局参数：coco测试集文件夹
  imgList: *lc_cocoval2017_list
  # 全局参数：由coco测试集文件夹中图片文件名列表保存成的txt
  names: *coconames
  # 全局参数：数据集类别名称文件，show=true时可以显示类别名称
  
# 需要修改的例子
  ddrnet:
    imgRoot: E:\mIoU_test\CITYSCAPES\
    # 测试集文件夹
    imgList: E:\mIoU_test\CITYSCAPES\valImages.txt
    # 由测试集文件夹中图片文件名列表保存成的txt
  resnet:
    imgRoot: E:\ImageNet2012_Test5000\images/
    # 测试集文件夹
    imgList: E:\ImageNet2012_Test5000\Test5000_imgList.txt
    # 由测试集文件夹中图片文件名列表保存成的txt
    names: E:\Gluon_ResNet\names\imagenet.names
    # 数据集类别名称文件，show=true时可以显示类别名称
    resize_hw: [256,256]
    # 前处理，与模型的浮点工程对齐即可，先resize，大部分分类网络不需要修改
    crop_hw: [224,224]
    # 前处理，resize之后中心裁剪，中心裁剪后即为网络input的大小，大部分分类网络不需要修改
```

## 6.位流盘挂载

> 以前我们更换新版本位流时，需要把sd卡从板子中取出，再插到windows电脑
>
> 而通过将FAT32的位流盘挂载到EXT4中，可以直接在linux系统中十分方便地更换位流
>
> 这里我们默认挂载位流盘的位置在/root/bits

### 6.1 **临时挂载位流盘**

```
sudo mkdir bits
sudo mount -t vfat /dev/mmcblk0p1 /root/bits
```

### 6.2 **永久挂载位流盘**（推荐）

- 首先我们查看本机的通用唯一识别码

```
sudo cp /etc/fstab /etc/fstab.backup
sudo blkid	# 查看通用唯一识别码
```

- 显示如下

```
/dev/mmcblk0p1: UUID="1EAD-86C0" TYPE="vfat" PARTUUID="f908429e-01"
/dev/mmcblk0p2: UUID="0c4acc08-e9b9-4f64-9545-66a79cfb16c7" TYPE="ext4" PARTUUID="f908429e-02"
```

- 这里的UUID="1EAD-86C0"就是我们后面需要用到的通用唯一识别码，不同的机器会有不同的UUID

```
cd /root
mkdir bits
sudo vim/etc/fstab
添加：
UUID=1EAD-86C0 /root/bits vfat defaults,umask=000 0 0	# 这里填写我们刚才查看获得的UUID
```

- 挂载已配置项目 `sudo mount -a`
- 检查位流是否已挂载到对应位置，如果正确把这句加到 `~/.bashrc`中

 **替换位流：**

`<img src = "assets/clip_image002.gif">`

`cd /root/bits`，然后从想用位流的文件夹中cp BOOT.bin到bits文件夹中，然后reboot

**注意有效文件就4个**（如图所示），其他可以删掉
