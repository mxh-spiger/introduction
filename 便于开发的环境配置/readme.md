# 开发软件、工具、流程、攻略等

说明主要分为两部分。

[第一部分](https://gitee.com/mxh-spiger/introduction/blob/master/便于开发的环境配置/Icraft开发流程相关.md)是Icraft开发流程相关的内容，包括框架版本、上板部署程序的编译环境、[仿真工程](https://gitee.com/mxh-spiger/simulate)的详细使用说明、icraft版本切换方式。

[第二部分](https://gitee.com/mxh-spiger/introduction/blob/master/便于开发的环境配置/其他推荐软件及环境准备.md)为其他开发环境，包括工具软件的安装说明、使用串口或网口的方式进行上板操作的流程，以及vscode通过gdb进行远程调试的配置方法。如果之前没有安装过相关的软件和配置，可以参考这一部分。

进入相关文件后可以点击左侧导航栏跳转至感兴趣的内容。

下图为各部分在完整使用流程环节中的作用示意

![image-20250224143858834](.\assets\image-20250224143858834.png)