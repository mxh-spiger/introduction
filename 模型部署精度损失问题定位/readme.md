# Icraft误差分析

## 名词解释

Icraft是复微智能计算平台基于自主研发的异构计算FPAI芯片的软件工具套件，提供Compiler、Simulator、Runtime工具，覆盖模型编译、仿真、运行时的全流程。

Icraft Compiler负责将第三方深度学习框架下训练得到的神经网络模型进行离线编译和优化，生成适合硬件部署的、执行高效的网络描述（json&raw文件）。

Icraft Simulator提供仿真功能，支持用户对模型编译的不同阶段进行仿真调试，帮助定位在模型编译过程中出现的问题、误差分析。

Icraft Runtime提供runtime、Device、forward等运行库来驱动AI硬件单元，完成编译后模型的板级部署。

详细情况请参考，icraft使用指南导航[![mainpage](assets/logo9-1706013858524.png)](https://gitee.com/mxh-spiger/icraft-introduction)



## 精度误差产生环节的定位流程

提到模型部署过程精度下降的问题，大家往往都会想到是训练后量化造成的误差。量化的确是精度误差的主要来源。但是模型部署需要经过若干环节，每个环节都可能引起误差。

![qt-stage](assets/qt-stage-1706017092673.gif)

主要有3大环节会对最终测试精度引入误差

- 模型导出：等效修改模型，并将框架训练好的weights转成静态图，导出可被AI编译器接受的模型
- 模型编译：使用icraft编译器将导出的模型编译成可部署到硬件的模型
- 运行时部署：调用运行时接口将编译后的模型部署到硬件完成推理

![pipeline](assets/pipeline.png)

![pipeline2](assets/pipeline2.png)

**误差分析原流程图**

当模型的最终精度结果与预期不符时，如何进行误差分析？这是很多用户的困扰。可能是模型导出过程出现问题，也可能是编译、部署过程中出现误差。Icraft误差分析手册将介绍以下内容，为用户提供从模型导出到板级部署的误差分析全流程指导！

[模型导出误差分析](模型导出误差分析.md)

[Icraft编译误差分析](Icraft编译误差分析.md)

[Icraft量化误差分析](Icraft量化误差分析.md)

[Icraft运行时误差分析](Icraft运行时误差分析.md)