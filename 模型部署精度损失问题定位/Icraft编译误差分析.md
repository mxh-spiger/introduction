# Icraft编译误差分析

# 1.**编译及部署流程**

Icraft是复微智能计算平台基于自主研发的异构计算FPAI芯片下的软件工具套件，可以将开源框架的网络模型转换为硬件支持的离线模型。

其中Icraft Compiler负责将深度学习框架下训练得到的神经网络模型进行编译，转换为硬件支持的离线模型。在模型转换过程中Icraft会对原始的深度学习模型进行转换及优化，包括计算图解析、算子调度优化、量化、数据重排、内存优化、指令生成等具体操作，从而生成满足硬件部署的、执行高效的网络描述（json&raw文件）。

编译及部署流程包括以下步骤：

1. TracedModel经过Parser解析后，转换为中间表示XIR;
2. XIR经过计算图解析、优化、量化、排布转换等一系列操作后，转换成适配硬件的离线模型;
3. 通过运行时API加载转换后的离线模型文件（json&raw）实现板级部署，运行时提供了runtime、Device、forward等运行库来驱动AI硬件单元，完成上板推理。

# 2.**关键概念**

**硬算子**

硬件执行计算（如Imk和IcorePost）。

imk:主要负责输入数据的搬移，从输入源搬移到pl-ddr。

备注：当前版本的imk硬算子仅支持输入通道为1-4，且第一个算子为卷积（不能为分组或深度可分离卷积）。

icorepost：负责硬件上对检测结果进行阈值筛选，将大于设定阈值的检测框结果输出，主要针对目标检测网络。

**数据排布格式**

一般情况下都是NHWC，非4D图片输入可能需要配置为FD

**归一化参数**

按照实际推理时的通道顺序配置每一个通道的归一化参数，如yolov5为RGB，yolox为BGR

**比特位宽**

表示部署时的定点位数，支持8bit、16bit

**总体约束**

在进行模型编译前，请务必查看以下约束要求

- 如果要将模型转成适配硬件的离线模型，请务必参见定制网络修改对模型进行修改和导出
- 支持原始框架为Pytorch、ONNX、Tensorflow、PaddlePaddle的模型转换
- 不支持动态shape的输入，如输入NHCW=[?,640,640,3] 但不支持输入NHCW=[?,?,?,3],模型编译时序固定数值

# 3.误差分析

## 3.1基本思路

Icraft 仿真组件（Icraft Simulator）提供仿真功能，支持用户对模型编译的各个步骤进行仿真调试，帮助定位在模型编译过程中出现的问题。

在固定输入数据后，可借助Icraft Simulator获得各阶段组件的输出结果，然后与GPU/CPU对比。

我们的误差分析思路是：**自顶向下、模块化分析**。按照网络的编译流程，依次排查网络解析、优化、量化、量化后优化、指令生成各个组件导致的误差。

## 3.2输入

### 输入个数

**单输入情况**

只有一个输入时，只需要在仿真配置的image字段输入文件路径即可

**多输入情况**

当网络需要读个输入时，需要在仿真配置的image字段**依次**输入多个文件路径，中间用分号间隔；或将所有输入依次写入一个文本文件中（一行一个，不需要分隔符），image字段为该文本文件路径

**多个输入文件的顺序必须与json文件中保持一致**，否则可能导致编译误差。

### 输入格式

底层使用opencv解析图片文件，仿真时编译的各个阶段图片均作为浮点处理。

- 输入为uint8数据类型，正常解析；
- 输入为uint16数据类型，需要将输入图像保存为uint16浮点类型的ftmp文件，如果传入uint16的图像会按照uint8处理，导致输入误差。
- 输入为ftmp，需要进行数据排布转化。

容易导致误差的点为，ftmp的数据排布是NHWC的，可以将其用numpy读⼊后先变成4维，然后可根据需要手动转化成NCHW等框架排布。

```python
#以ftmp输入为例
ftmp_140 = np.fromfile(R'./sim_140.ftmp', dtype=np.float32)
outputs_1 = torch.tensor(ftmp_140 .reshape(1, 80, 80,255)).permute(0, 3, 1, 2).contiguous()
```

### 归一化参数

- 归一化参数pre-mean/pre-scale应该与框架下的归一化方式保持一致，一般位于数据集/数据的前处理流程中（如preprocess_image、normalize_image）

```python
# yolov5 前处理
img = img.transpose((2, 0, 1))[::-1] # HWC to CHW, BGR to RGB
img = np.ascontiguousarray(img)
im = torch.from_numpy(img).float().unsqueeze(0)
im /= 255
```

上述为yolov5的前处理流程，根据$image = (image - mean)/scale$，则pre_mean = 0, pre_scale = 255。

```python
# SeResNet 前处理
data_transform = transforms.Compose(
        [
         transforms.Resize(256),
         transforms.CenterCrop(224),
         transforms.ToTensor(),
         transforms.Normalize([0.485,0.456,0.406], [0.229,0.224,0.225])])
```

上述为SeResNet的前处理流程，已经进行了ToTensor()归一化处理到[0,1](将数据除以255)，再进行标准化。根据$image = (image - mean)/scale$，则pre_mean = [123.675,116.28,103.53], pre_scale = [58.395, 57.12, 57.375]。

- 输入为ftmp文件时，不会为输入算子进行前处理；如果输入有前处理，需要在存成ftmp之前自行完成前处理。
- 输入为图像文件时，会按照pre-mean/pre-scale字段的配置对图像进行前处理。

需要确保pre-mean/pre-scale字段的配置与浮点前向中的前处理一致，否则会导致输入误差。

### channel_swap

- ps_in输入时，默认以BGR格式读入图像
- pl_in输入时，camera默认以RGB格式读入图像

因此需要根据输入是否需要进行BGR转RGB来正确配置channel_swap字段，否则会引起输入误差。

- 以yolov5为例，在数据前处理过程中以RGB格式读入图像， pl_in输入时需要转为BGR格式，因此channel_swap = [2,1,0]
- 以yolox为例，在数据前处理过程中以BGR格式读入图像，pl_in输入时无需转换，因此channel_swap = [0,1,2]

需要根据框架下图像的格式确保channel_swap字段的配置正确，否则会导致输入误差。

网络输入与源python工程不一致往往是引起误差的一大原因，因此出现编译误差时建议着重分析此部分。

![2_1](assets/Icraft2_1.png)

## 3.3组件

### 定位组件误差

组件仿真一共有5个阶段，分别是：**解析后的浮点仿真（parsed）、优化后的浮点仿真(optimized)、量化后的定点仿真(quantized)、适配后的定点仿真(adapted)、指令生成后的指令仿真(generated)**。

**通过对相邻组件的编译结果进行仿真，可以确定误差来源于哪个编译阶段。**

- 示例1:parsed阶段仿真结果与浮点结果不一致，说明问题来自编译阶段。此时可检查parsed阶段的参数设置，如框架版本、pre-mean、pre-scale、输入格式（nhwc还是nchw）、输入大小（如果输入图像与网络输入不一致，需要将pre-method设置为resize），是否需要进行通道转换（channel_swap）。
- 示例2：optimized阶段仿真结果与quantized阶段仿真结果不一致，说明问题可能来自量化阶段。此时可借助量化分析工具分析误差是否由量化导致，具体分析过程可参考[Icraft量化误差分析](Icraft量化误差分析.md)。
- 示例3：generated阶段仿真结果与adapted阶段仿真结果不一致，说明问题可能来自指令生成阶段。此时可先排查fake_qf字段，确定是否由fake_qf导致。

### fake_qf

背景：

在实际硬件前向中，会将输入切分为若干个小块分块，每个小块独立进行量化操作（截位、饱和等）。仿真器模拟了这一行为，因此generated阶段的仿真结果与实际上板结果保持一致。

adapted阶段并不会分块计算，而是将全部数据计算完成后作为一个整体进行量化操作，因此导致adapt仿真与指令仿真结果存在差异，无法直接对比，需要对fake_qf字段进行设置。

fake_qf设置：

fake_qf表示强制关闭前向时的量化操作，默认设置为false。fake_qf设置为true，特征图将不再进行截位饱和等操作，但其他操作正常进行。

解决方案：

1. 当需要将adapt与generated进行仿真对比时，需要将fake_qf设置为true，表示两个阶段均不进行量化。此时可以将adapt仿真结果作为golden，用来测试指令生成阶段是否正确。
2. 当需要将generated结果与实际上板结果进行对比时，需要将fake_qf设置为false，表示两个阶段均进行量化，代表真实结果。

需要根据实际的对比需求，对fake_qf进行正确设置，否则会导致二者结果不一致，引起误差。

### dump_ftmp

dump_ftmp支持不同模式的数据排布

S： software数据排布，在parsed、optimized、quantized、adapted阶段使用，当前有 NxHWxC、NxC两种排布格式

H：hardware数据排布，在adapted、generated阶段使用，当前有NxCtxHxWxCu、NxCtxCu两种格式

F：非量化状态下的原始float数据格式

Q：量化状态下quantized的数据格式，8bit量化下数据排布在-128-127整数范围内，16bit量化下数据在-32768-32676整数范围内

T：txt数据格式

B：binary的数据格式

如dump_ftmp = "SFB” 则表示 software数据排布、非量化状态下的原始float数据格式、binary的数据格式。

在进行组件间对比分析时，要确定二者的数据排布才可进行对比，否则会由于输出数据格式不一致导致出现误差。

➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖


下一节： [Icraft量化误差分析](Icraft量化误差分析.md)   
返回主页： [Icraft误差分析](Icraft误差分析手册.md)