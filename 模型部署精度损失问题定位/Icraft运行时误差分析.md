# Icraft运行时误差分析

# 1.部署流程

Icraft Runtime提供一系列运行时API，帮助用户运行编译生成的json&raw文件，完成模型的部署、运行、结果获取等功能。

Icraft Runtime组件实现模型的上板运行流程为：

打开设备→设置参数→前处理→设置输入图片→运行前向→后处理→关闭设备。

![3_1](assets/Icraft4_1.png)

# 2.关键概念

**Runtime Tensor** 

通过传入图片、ftmp、二进制文件，来构造Host端或Device端的数据指针，同时支持Host端和Device端的数据转换与转移

**Runtime Network**

创建网络结构，支持获取算子集合、特定算子

**Runtime forward**

执行网络前向

**数据流**

Runtime Tensor分为Host端和Device端。

 Host端RuntimeTensor内部有ps端内存的实际数据指针，用户可以通过API直接得到；

Device端RuntimeTensor内部有pl端ddr地址，通过与icore的同步机制，确保icore能将计算完成的数据写回pl ddr，**用户需要通过Device相关接口将pl ddr上的数据读到ps内存中**。

**数据排布**

Host端和Device端的数据排布存在差异，Host端的数据排布通常为(n,h,w,c)，Device端的数据排布通常为(n,ct,h,w,cu) 其中ct为总通道切tile数，cu为每tile的通道数。

# 3.误差分析

## 3.1基本思路

我们的误差分析思路是：**顺序分析**。按照网络的部署流程，依次排查网络参数设置、前处理方式、输入图像、网络前向、后处理方式可能导致的误差。

## 3.2 参数设置

为了能够正确获得上板部署结果，需要提前设置一些参数用于网络推理。参数来自浮点工程、编译过程等，关系到能否获得正确的推理结果，也是很大程度导致误差产生的原因，可以按照下述参数检查是否设置正确：

```python
conf:检测结果的置信度阈值    #这一参数十分重要，如测试精度时需要将其设置为与浮点工程保持一致（如0.001）,否则将导致测试精度与浮点存在误差
iou:NMS时的交并比阈值       #需要与浮点工程保持一致
number_of_class: 检测类别数目
number_of_head:检测头的数目  #anchor based类的检测网络需要设置
anchors:锚框信息            #anchor based类的检测网络需要设置，需要与浮点保持一致
multilable:是否进行multilabel检测 #精度测试时设置为true
```

以yolov5为例，模型的参数设置为：

```python
conf = 0.001 #精度测试用， 常规可视化可设置为0.2
iou = 0.65
number_of_classes = 80
number_od_head = 3
anchors = [[[10.,13.],[16.,30.],[33.,23.]],[[30.,61.],[62.,45.],[59.,119.]],[[116.,90.],[156.,198.],[373.,326.]]]
multilable = true
```

## 3.3 前处理

前处理需要与源python工程浮点计算过程保持一致。

- 示例1：浮点计算中输入图像经过resize操作，则前处理需要同样进行resize操作；如果进行了pad操作，则前处理也同样需要进行pad,目的是确保输入与源python工程的输输入处理过程一致，**除了不进行pre-mean\pre-scale操作**。

```cpp
PRE::PicPre img(img_path);
img.Resize(netinfo.input_hw, PRE::PicPre::LONG_SIDE).rPad();
```

- 示例2：浮点计算中输入图像为uint16数据类型，则前处理需要同样以uint16的格式读图;如果按照uint8读图会导致送入网络的输入数据错误，引起误差。

```cpp
cv::Mat img = cv::imread(IMG_PATH, cv::IMREAD_COLOR);       // 读取uint8图片
cv::Mat img = cv::imread(IMG_PATH, cv::IMREAD_ANYDEPTH);    // 读取uint16图片
```

## 3.4 网络输入

网络输入分为以下几种类型：RGB图像、非RGB图像、ftmp。

- 示例1：RGB图像

如果输入为RGB图像，则输入图像要完成前处理除了pre-mean/pre-scale的其他操作。

- 示例2：非RGB图像

如果输入为非RGB图像，如单通道图像、四通道图像，则在**编译和上板**过程中需要进行对应格式的decoder。

以单通道图像为例：

```cpp
//compile需要配置
decode_dll = dll/GRAYDecoder_float.dll
//部署工程读图方式
PicPre img(img_path, cv::IMREAD_GRAYSCALE);
```

- 示例3：ftmp的输入适配。

如果输入为非rgb图片，而是ftmp的形式，则无法进行pre-mean/pre-scale操作，因此ftmp中应是输入数据进行完前处理和pre-mean/pre-scale后的结果，再传入网络中。这一点与rgb图像输入有差异，请注意。

```cpp
img_tensor = icraft::rt::RuntimeTensor::fromFtmpFile(FTMP_PATH,{1,224,224,3},32,true); //输入为ftmp
std::vector<std::shared_ptr<icraft::rt::RuntimeTensor>> tensor_result = runtime.forward({ img_tensor });
```

**网络输入与源python工程不一致往往是引起误差的一大原因，因此出现误差时建议着重分析此部分。**

## 3.5 硬件阈值筛选算子

### 3.5.1 icorepost作用

icorepost会直接将大于阈值的数据传到psddr；并且传输的数据为量化后数据

- 实际使用需要将icorepost结果进行**反量化**
- 取出有效数据
- icorepost的参数设置直接影响输出的网络结果

### 3.5.2 icorepost排查点

icorepost负责硬件阈值筛选，主要接在检测网络输出后面，需要结合检测网络输出层的排布特点，检查/config/customop/model_customop.ini中的参数设置。

Icorepost主要检查以下参数：

- 阈值
- 做icorepost的数据一共有几个head、需要分为几组（group）
- 每个grid cell对应的anchor个数
- 做阈值筛选的score所在通道（从1开始）

```cpp
# 以anchor-based的yolov5为例
thr_f = 0.1 #硬件筛选阈值，测精度时改为0.001
cmp_en = 1  #是否需要做阈值比较

#下述参数根据不同网络设置不同参数
groups = 3 #表示有几个输出head(注意：不是输出层的个数，比如yolox有3个head,9个输出层)
anchor_num = 3 # 每个cell对应⼏个anchor；anchor free的设为1
position = 5 # 代表需要筛选的阈值所在的位置。例如，yolov5的score是在第5个通道，则设置为5
```

```cpp
# 以anchor-free的yolov6为例
thr_f = 0.1 #硬件筛选阈值，测精度时改为0.001
cmp_en = 1  #是否需要做阈值比较

#下述参数根据不同网络设置不同参数
groups = 3 #表示有几个输出head(注意：不是输出层的个数，比如yolox有3个head,9个输出层)
anchor_num = 1 # 每个cell对应⼏个anchor；anchor free的设为1
position = all # 代表需要筛选的阈值所在的位置
```

## 3.6 网络输出

- 如果json文件最后一层是icraft内部的后处理函数（如sigmoid、icorepost）可以直接调用runtimeTensor的接口得到输出结果

```cpp
auto result = runtime_result[0].getResult();
```

- 如果json文件最后一层并非网络最终输出，则需要将输出结果进行**反量化**，才能得到浮点结果，从而进一步在host端接后处理。

```cpp
auto result = runtime_result[0].getResult();
for (int i = 0; i < result.size(), ++i){
	float_output = result[i] * scale[i]; //反量化
}
```

**没有进行合适的反量化也是导致误差的一大原因**。如分类网络最后一层Linear作为网络输出，为定点结果，需要进行反量化后才能接后处理。

## 3.7 后处理

- yolo系列输出格式

yolo系列网络的输出为最后一层conv的输出。需要正确理解yolo系列网络的输出格式才能进一步进行结果的后处理。

- 1.有无score ：yolov3/4/5/7的输出包含score信息； yolov6/8的输出不包含score信息，通过遍历类别概率判断是否存在物体
- 2.是否decouple：yolov5的输出为couple型，共3个输出层，分别代表large/middle/small bbox； yolox的输出为 decouple型，在3个head中各自包含3个输出层，因此共9层输出。
- 3.是否anchor free: anchor based网络其bbox结果需要设置先验anchor来计算；anchor free网络不需要设置先验anchor
- 4.icorepost硬件输出格式除了前85个常规的xywh检测信息，还包括$loc_x$、$loc_y$、$idx$等硬件适配的检测结果，需要按照输出格式正确解码出相关信息，转化为最终的检测结果（bbox_list,score_list,id_list）。

下面以yolov5为例，展示正确的后处理流程。

```python
# ---------------------------------后处理---------------------------------
# Step 1: 读取norm_ratio，对定点结果进行反量化
with open(GENERATED_JSON_FILE,'r') as f:
    net = json.load(f)
scale_list = [] 
# 从json文件中获取输出的 norm_ratio 用于Step 2中的反量化
for ftmp in net["ops"][-2]["inputs"]:
    scale_list.append(ftmp["dtype"]["element_dtype"]["normratio"][0]["value"])
print('INFO: get scale results!')
# Step 2：将icore_post数据排布的结果转化为box_list,scores_list,id_list
id_list = []
scores_list = []
box_list = []
icore_post_res = []
# flatten icore_post_result
for i in range(len(generated_output)):
    output = np.array(generated_output[i]).flatten()#模型中数据排布 e.g [1,1,133,96] ->[133*96]
    icore_post_res.append(output)
for i in range(len(icore_post_res)):
    objnum = icore_post_res[i].shape[0] / ANCHOR_LENGTH    
    tensor_data = icore_post_res[i]
    # 根据输出格式获取正确的idx、location_x、location_y信息
    for j in range(int(objnum)):
        obj_ptr_start = j * ANCHOR_LENGTH
        obj_ptr_next = obj_ptr_start + ANCHOR_LENGTH
        if BIT==16:
            anchor_index = tensor_data[obj_ptr_next - 1]
            location_y = tensor_data[obj_ptr_next - 2]
            location_x = tensor_data[obj_ptr_next - 3]
        elif BIT==8:
            anchor_index1 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 1]
            anchor_index2 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 2]
            location_y1 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 3]
            location_y2 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 4]
            location_x1 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 5]
            location_x2 = tensor_data[j * ANCHOR_LENGTH + ANCHOR_LENGTH - 6]
            anchor_index = (anchor_index1 << 8) + anchor_index2
            location_y = (location_y1 << 8) + location_y2
            location_x = (location_x1 << 8) + location_x2
				# 结果反量化
        _x = sigmoid(tensor_data[obj_ptr_start ]    * scale_list[i])
        _y = sigmoid(tensor_data[obj_ptr_start + 1] * scale_list[i])
        _w = sigmoid(tensor_data[obj_ptr_start + 2] * scale_list[i])
        _h = sigmoid(tensor_data[obj_ptr_start + 3] * scale_list[i])
        _s = sigmoid(tensor_data[obj_ptr_start + 4] * scale_list[i])
        
        class_ptr_start = obj_ptr_start + 5
        class_data_list = tensor_data[obj_ptr_start + 5:obj_ptr_start +5+N_CLASS]
        max_value = max(class_data_list)
        max_idx = list(class_data_list).index(max_value)
        realscore = _s / (1 + np.exp( - max_value * scale_list[i ]))

        x = (2*_x + location_x-0.5) * STRIDE[i]
        y = (2*_y + location_y-0.5) * STRIDE[i]
        w = 4 * (_w)**2  * ANCHORS[i][anchor_index][0]
        h = 4 * (_h)**2  * ANCHORS[i][anchor_index][1]

        scores_list.append(realscore)
        box_list.append(((x - w / 2), (y - h / 2), w, h))
        id_list.append(max_idx)

# Step 3: NMS 取NMS后的结果
nms_indices = soft_nms(box_list, scores_list, id_list, THR_F, IOU, N_CLASS)
# 接后续的检测结果可视化等。
```

## 3.8 上板结果与仿真结果对比

如果获得了板级部署结果，可以进一步与Icraft编译仿真的结果进行对比，判断上板结果与仿真结果是否一致。

Icraft编译仿真结果可参考[https://www.notion.so/Icraft-bd54b46040bf42aababf464f271aaa01?pvs=4](Icraft%E7%BC%96%E8%AF%91%E8%AF%AF%E5%B7%AE%E5%88%86%E6%9E%90%20bd54b46040bf42aababf464f271aaa01.md) 对指令生成阶段（generated）进行仿真获得。

上板部署结果可将pl ddr中的输出数据保存成ftmp的形式（dump ftmp），与仿真结果进行对比。

如果结果不一致，则误差原因可能来自硬件，也可能来自指令生成阶段。

➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
   

返回主页： [Icraft误差分析](Icraft误差分析手册.md)